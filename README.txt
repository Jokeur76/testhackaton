﻿pour cloner le repo disatnt en local : 
git clone adresse (ex en https : git clone https://MickLarch@bitbucket.org/MickLarch/testhackaton.git)
sur bitbucket : aller à l'adresse https://bitbucket.org/MickLarch/testhackaton et cliquer sur clone. Sélectionner le type de connection que vous souhaitez (https/ssh) et copiez la commande

Tentative

connexion en ssh : 
avantage : pas de mot de passe à taper (si pas de passphrase ou si un ssh agent est parametré) donc gain de temps en restant sécurisé
- créer un jeu de clé sur son pc (linux : cd ~/.ssh && ssh-keygen, mon avis : pas de passphrasse)
- copier le contenu de id_rsa.pub (si vous avez laissé le nom de fichier par défaut) qui est la clé publique
- coller cette clé publique dans bitbucket profil (l'avatar en haut a droite) > bitbucket settings > SSH keys
- cloner le repo avec git clone git@bitbucket.org:MickLarch/testhackaton.git
- utiliser les commandes : pas besoin de mot de passe o/

**Cinématique**
1- les fichiers et leur modifications ne sont pas "suivi" (staged) : git connait le fichier mais le prends pas en compte. Lors d'un push (cf plus loin) le fichier n'est pas envoyé sur le serveur
2- git add : permet de suivre les fichiers. Ils sont ajouté au projet en quelque sorte. Les modifications sont prise en compte.
3- git commit : on valide les modifications. peut se faire par fichier (git commit FICHIER), ou pour tous les fichiers suivis (git commit --all)
4- git pull : permet de récupérer (tirer à soi) les modifications présente sur le serveur. a faire avant de push si on travaille a plusieurs moins important si on travaille tout seul.
5- git push : envoyé ses modifications sur le serveur. Pour cela (à vérifier) il faut que l'on soit a jour par rapport au serveur et que tous les fichiers suivi soit commités

***************************************
**Commandes principales (à completer)**
***************************************

git clone ADDRESS : copie le repo sur votre disque dans un dossier du nom du repo
git add FICHIER : mettre FICHIER dans le suivi
git add --all: mettre tous les fichiers dans le suivi
git commit FICHIER : valider les modifications de FICHIER
git commit --all : valider les modifications de tous les fichiers suivis. Ouvre un editeur de texte : on rentre le message de comit on enregistre et c'est commité)
git commit --all -m "MESSAGE" : valider les modifications de tous les fichiers suivis avec le message de commit sans passser par l'éditeur (on perds les message automatique donc pas top)
git pull
git push

git log : affiche les derniers commits avec le hash (plus ou moins un id) et le message.

#les branches : très utile pour travailler a plusieurs sur les mêmes fichiers
git branch BRANCHE : créé une branche du nom de BRANCHE. Les fichiers seront ceux courant même s'il ne sont pas ajoutés au suivi ou commité (pratique quand on a oublié de créer une branche et qu'on veux pas perdre les modifs)
git checkout BRANCHE : se rends sur la branche BRANCHE (je crois qu'il faut avoir commité ses modifs, soit le checkout est refusé soit on perds le modifs à vérifier)
git checkout -b BRANCHE : se rends sur la branche BRANCHE et la créé si elle n'existe pas, même principe que git branche on embarque les modifs

git merge : fusionne deux branches. Peut créer des conflits non gérés automatiquement, je conseille un bon IDE pour gérer les conflits
git rebase BRANCHE : rejoue les commits de la branche courant à la fin de la branche BRANCHE : ex: 
timeline
 | branch1 : 1-2-3
 | git checkout -b branch2
 | branch2: 1-2-3
 | 3 commits
 | branch2: 1-2-3-a-b-c
 | git checkout branch1
 | branch1 : 1-2-3-4-5
 | git checkout branch2
 | git rebase branch1
 | branch2 : 1-2-3-4-5-a-b-c

#le remisage : ou comment mettre de coter ses modifs sans devoir ni les commiter ni les perdres
git stash : prends toutes les modifications depuis le dernier commit et les stocks (plusieurs stash a suivre possibles il seront tous gardés)
git stash apply : récupère les dernière modifs satshée sans les supprimer
git stash pop : permet de récupéré les dernieres modifs stashées et de les supprimer (prends dans l'ordre : le premier pop ressort le dernier stash le deuxieme pop ressort l'avant dernier stash etc)
git stash apply stash@{0} : récupérer le stash avec l'id 0 (idem avec pop)
(plus d'info sur stash/le remisage : https://ariejan.net/2008/04/23/git-using-the-stash/ ou https://www.git-scm.com/docs/git-stash)



